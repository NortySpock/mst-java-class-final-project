public class Unit
{
  public Unit(String quantity, String name, double conversionFactor)
  {
    m_quantity = quantity;
    m_name = name;
    m_conversionFactor = conversionFactor;
  }
  
  public String getQuantity()
  {
    return m_quantity;
  }
  
  public String getName()
  {
    return m_name;
  }
  
  public Double getConversionFactor()
  {
    return m_conversionFactor;
  }
    
  private String m_quantity; //Quantity being measured e.g. mass, length, etc.
  private String m_name; //Human readable name e.g. meter, foot-pounds, cm per second
  private double m_conversionFactor; //What you have to multiply this by to get the SI unit
}
