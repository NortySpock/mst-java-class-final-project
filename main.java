import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.ArrayList;


public class main
{
  public static final int WIDTH = 800;
  public static final int HEIGHT = 200;

  public static ArrayList<Unit> unitList = new ArrayList<Unit>();
  
  static DefaultListModel dimensionList = new DefaultListModel();
  static DefaultListModel unitFromList = new DefaultListModel();
  static DefaultListModel unitToList = new DefaultListModel();
  static JList dimensionListGUI = new JList (dimensionList);
   
  static JFrame mainWindow = new JFrame();
  
  public static void main(String [] args)
  {
    setupModel();
    
    setupView();
    
    mainWindow.setVisible(true);
  }
  
  protected static void setupView()
  {
    mainWindow.setSize(WIDTH, HEIGHT);
    mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    mainWindow.setLayout(new FlowLayout());
   
    //Create and populate the dimension panel   
    JPanel dimensionPanel = new JPanel();
    dimensionPanel.setLayout(new BorderLayout());
    
    JLabel dimensionLabel = new JLabel("Dimension");
    dimensionPanel.add(dimensionLabel, BorderLayout.NORTH);
    

    dimensionList.addElement("mass");
    dimensionList.addElement("length");
    dimensionList.addElement("time");


    dimensionListGUI.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
    dimensionListGUI.setLayoutOrientation(JList.VERTICAL);
    dimensionListGUI.setVisibleRowCount(7);

    JScrollPane dimensionScrollPane = new JScrollPane(dimensionListGUI);
 
    dimensionPanel.add(dimensionScrollPane, BorderLayout.CENTER);
    
    
    //Create the unitFromPanel
    JPanel unitFromPanel = new JPanel();
    unitFromPanel.setLayout(new BorderLayout());
    

    JList unitFromListGUI = new JList (unitFromList);
    unitFromListGUI.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
    unitFromListGUI.setLayoutOrientation(JList.VERTICAL);
    unitFromListGUI.setVisibleRowCount(7);
    
    JScrollPane unitFromScrollPane = new JScrollPane(unitFromListGUI);
    
    unitFromPanel.add(unitFromScrollPane, BorderLayout.CENTER);
    
    JTextField unitFromTextField = new JTextField(10);
    
    unitFromPanel.add(unitFromTextField, BorderLayout.SOUTH);
    
    //Create and populate arrow panel
    ImageIcon arrowIcon = new ImageIcon("arrow.png");
    JLabel arrowLabel = new JLabel(arrowIcon);
    JPanel arrowPanel = new JPanel();
    arrowPanel.add(arrowLabel);
    
    //Create the unitToPanel
    JPanel unitToPanel = new JPanel();
    unitToPanel.setLayout(new BorderLayout());


    JList unitToListGUI = new JList (unitToList);
    unitToListGUI.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
    unitToListGUI.setLayoutOrientation(JList.VERTICAL);
    unitToListGUI.setVisibleRowCount(7);
    
    JScrollPane unitToScrollPane = new JScrollPane(unitToListGUI);
    
    unitToPanel.add(unitToScrollPane, BorderLayout.CENTER);
    
    JTextField unitToTextField = new JTextField(10);
    
    unitToPanel.add(unitToTextField, BorderLayout.SOUTH);
    
    //Add it all to the main window    
    mainWindow.add(dimensionPanel);
    mainWindow.add(unitFromPanel);
    mainWindow.add(arrowPanel);
    mainWindow.add(unitToPanel);
  }
  
  protected static void setupModel()
  {

    unitList.add(new Unit("mass", "kilogram", 1.0d));
    unitList.add(new Unit("mass", "pound", 0.45359237d));
    unitList.add(new Unit("mass", "slug", 14.5939d));

    unitList.add(new Unit("length", "meter", 1.0d));
    unitList.add(new Unit("length", "foot", 0.3048d));
    unitList.add(new Unit("length", "yard", 0.9144d));

    unitList.add(new Unit("time", "second", 1.0d));
    unitList.add(new Unit("time", "hour", 3600.0d));
    unitList.add(new Unit("time", "day", 86400.0d));

  }
  
  protected class dimensionListener implements ListSelectionListener 
  {
    public void valueChanged(ListSelectionEvent e)
    {
        if (e.getValueIsAdjusting() == false) //done adjusting dimension list
        {
          unitFromList.clear();
          unitToList.clear();
          //populate unitFrom and unitTo lists
          for(Unit i : unitList)
          {
            System.out.print("Cycled a unit.");
            //Selected dimension exists in unit list
            if(dimensionList.get(dimensionListGUI.getSelectedIndex()).equals( i.getQuantity() ));
            {
              System.out.print(i.getName());
//              unitFromList.add(i.getName());
//              unitFromList.add(i.getName());
            }
          }
        }
    }
  }
  
}

